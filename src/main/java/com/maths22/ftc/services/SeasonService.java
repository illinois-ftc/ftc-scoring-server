package com.maths22.ftc.services;

import com.googlecode.jsonrpc4j.JsonRpcService;
import com.maths22.ftc.entities.Season;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.UUID;

/**
 * Created by jburroughs on 7/10/16.
 */
@JsonRpcService("/seasonService")
public interface SeasonService {
    @RequestMapping(method = RequestMethod.GET)
    List<Season> getSeasons();

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    Season getSeason(@PathVariable UUID id);
}
