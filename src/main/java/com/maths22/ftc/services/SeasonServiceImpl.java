package com.maths22.ftc.services;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import com.maths22.ftc.entities.Season;
import com.maths22.ftc.exceptions.NotFoundException;
import com.maths22.ftc.repositories.SeasonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by jburroughs on 7/6/16.
 */
@AutoJsonRpcServiceImpl
@Component
public class SeasonServiceImpl implements SeasonService {

    @Autowired
    private SeasonRepository seasonRepository;

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public List<Season> getSeasons() {
        ArrayList<Season> seasons = new ArrayList<>();
        seasonRepository.findAll().forEach(seasons::add);
        return seasons;
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Season getSeason(@PathVariable UUID id) {
        Season ret = seasonRepository.findOne(id);
        if(ret == null) {
            throw new NotFoundException();
        }
        return ret;
    }

}
