package com.maths22.ftc.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Arrays;
import java.util.UUID;

/**
 * Created by jburroughs on 7/6/16.
 */
@Entity
public class Score {
    private UUID id;
    private String majorPenaltiesAwarded;
    private String minorPenaltiesAwarded;
    private UUID seasonScoreId;
    private Season season;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", nullable = false)
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Basic
    @Column(name = "major_penalties_awarded", nullable = false, length = 45)
    public String getMajorPenaltiesAwarded() {
        return majorPenaltiesAwarded;
    }

    public void setMajorPenaltiesAwarded(String majorPenaltiesAwarded) {
        this.majorPenaltiesAwarded = majorPenaltiesAwarded;
    }

    @Basic
    @Column(name = "minor_penalties_awarded", nullable = false, length = 45)
    public String getMinorPenaltiesAwarded() {
        return minorPenaltiesAwarded;
    }

    public void setMinorPenaltiesAwarded(String minorPenaltiesAwarded) {
        this.minorPenaltiesAwarded = minorPenaltiesAwarded;
    }

    @Basic
    @Column(name = "season_score_id", nullable = false)
    public UUID getSeasonScoreId() {
        return seasonScoreId;
    }

    public void setSeasonScoreId(UUID seasonScoreId) {
        this.seasonScoreId = seasonScoreId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Score score = (Score) o;

        if (id != null ? !id.equals(score.id) : score.id != null) return false;
        if (majorPenaltiesAwarded != null ? !majorPenaltiesAwarded.equals(score.majorPenaltiesAwarded) : score.majorPenaltiesAwarded != null)
            return false;
        if (minorPenaltiesAwarded != null ? !minorPenaltiesAwarded.equals(score.minorPenaltiesAwarded) : score.minorPenaltiesAwarded != null)
            return false;
        return seasonScoreId != null ? seasonScoreId.equals(score.seasonScoreId) : score.seasonScoreId == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (majorPenaltiesAwarded != null ? majorPenaltiesAwarded.hashCode() : 0);
        result = 31 * result + (minorPenaltiesAwarded != null ? minorPenaltiesAwarded.hashCode() : 0);
        result = 31 * result + (seasonScoreId != null ? seasonScoreId.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "season", referencedColumnName = "id", nullable = false)
    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }
}
