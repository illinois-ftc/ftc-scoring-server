package com.maths22.ftc.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Arrays;
import java.util.UUID;

/**
 * Created by jburroughs on 7/6/16.
 */
@Entity
@Table(name = "season_score_resq", schema = "ftc_scoring", catalog = "")
public class SeasonScoreResq {
    private UUID id;
    private int r1AutoPos;
    private int r2AutoPos;
    private int autoClimbers;
    private int beaconsTriggered;
    private int debrisFloor;
    private int debrisLow;
    private int debrisMiddle;
    private int debrisHigh;
    private int r1DriverPos;
    private int r2DriverPos;
    private int driverClimbers;
    private int climbersReleased;
    private int hungRobots;
    private int allClear;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", nullable = false)
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Basic
    @Column(name = "r1_auto_pos", nullable = false)
    public int getR1AutoPos() {
        return r1AutoPos;
    }

    public void setR1AutoPos(int r1AutoPos) {
        this.r1AutoPos = r1AutoPos;
    }

    @Basic
    @Column(name = "r2_auto_pos", nullable = false)
    public int getR2AutoPos() {
        return r2AutoPos;
    }

    public void setR2AutoPos(int r2AutoPos) {
        this.r2AutoPos = r2AutoPos;
    }

    @Basic
    @Column(name = "auto_climbers", nullable = false)
    public int getAutoClimbers() {
        return autoClimbers;
    }

    public void setAutoClimbers(int autoClimbers) {
        this.autoClimbers = autoClimbers;
    }

    @Basic
    @Column(name = "beacons_triggered", nullable = false)
    public int getBeaconsTriggered() {
        return beaconsTriggered;
    }

    public void setBeaconsTriggered(int beaconsTriggered) {
        this.beaconsTriggered = beaconsTriggered;
    }

    @Basic
    @Column(name = "debris_floor", nullable = false)
    public int getDebrisFloor() {
        return debrisFloor;
    }

    public void setDebrisFloor(int debrisFloor) {
        this.debrisFloor = debrisFloor;
    }

    @Basic
    @Column(name = "debris_low", nullable = false)
    public int getDebrisLow() {
        return debrisLow;
    }

    public void setDebrisLow(int debrisLow) {
        this.debrisLow = debrisLow;
    }

    @Basic
    @Column(name = "debris_middle", nullable = false)
    public int getDebrisMiddle() {
        return debrisMiddle;
    }

    public void setDebrisMiddle(int debrisMiddle) {
        this.debrisMiddle = debrisMiddle;
    }

    @Basic
    @Column(name = "debris_high", nullable = false)
    public int getDebrisHigh() {
        return debrisHigh;
    }

    public void setDebrisHigh(int debrisHigh) {
        this.debrisHigh = debrisHigh;
    }

    @Basic
    @Column(name = "r1_driver_pos", nullable = false)
    public int getR1DriverPos() {
        return r1DriverPos;
    }

    public void setR1DriverPos(int r1DriverPos) {
        this.r1DriverPos = r1DriverPos;
    }

    @Basic
    @Column(name = "r2_driver_pos", nullable = false)
    public int getR2DriverPos() {
        return r2DriverPos;
    }

    public void setR2DriverPos(int r2DriverPos) {
        this.r2DriverPos = r2DriverPos;
    }

    @Basic
    @Column(name = "driver_climbers", nullable = false)
    public int getDriverClimbers() {
        return driverClimbers;
    }

    public void setDriverClimbers(int driverClimbers) {
        this.driverClimbers = driverClimbers;
    }

    @Basic
    @Column(name = "climbers_released", nullable = false)
    public int getClimbersReleased() {
        return climbersReleased;
    }

    public void setClimbersReleased(int climbersReleased) {
        this.climbersReleased = climbersReleased;
    }

    @Basic
    @Column(name = "hung_robots", nullable = false)
    public int getHungRobots() {
        return hungRobots;
    }

    public void setHungRobots(int hungRobots) {
        this.hungRobots = hungRobots;
    }

    @Basic
    @Column(name = "all_clear", nullable = false)
    public int getAllClear() {
        return allClear;
    }

    public void setAllClear(int allClear) {
        this.allClear = allClear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SeasonScoreResq that = (SeasonScoreResq) o;

        if (r1AutoPos != that.r1AutoPos) return false;
        if (r2AutoPos != that.r2AutoPos) return false;
        if (autoClimbers != that.autoClimbers) return false;
        if (beaconsTriggered != that.beaconsTriggered) return false;
        if (debrisFloor != that.debrisFloor) return false;
        if (debrisLow != that.debrisLow) return false;
        if (debrisMiddle != that.debrisMiddle) return false;
        if (debrisHigh != that.debrisHigh) return false;
        if (r1DriverPos != that.r1DriverPos) return false;
        if (r2DriverPos != that.r2DriverPos) return false;
        if (driverClimbers != that.driverClimbers) return false;
        if (climbersReleased != that.climbersReleased) return false;
        if (hungRobots != that.hungRobots) return false;
        if (allClear != that.allClear) return false;
        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + r1AutoPos;
        result = 31 * result + r2AutoPos;
        result = 31 * result + autoClimbers;
        result = 31 * result + beaconsTriggered;
        result = 31 * result + debrisFloor;
        result = 31 * result + debrisLow;
        result = 31 * result + debrisMiddle;
        result = 31 * result + debrisHigh;
        result = 31 * result + r1DriverPos;
        result = 31 * result + r2DriverPos;
        result = 31 * result + driverClimbers;
        result = 31 * result + climbersReleased;
        result = 31 * result + hungRobots;
        result = 31 * result + allClear;
        return result;
    }
}
