package com.maths22.ftc.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

/**
 * Created by jburroughs on 7/6/16.
 */
@Entity
public class Match {
    private UUID id;
    private int type;
    private int number;
    private Alliance blueAlliance;
    private Alliance redAlliance;
    private Division division;
    private Score redScore;
    private Score blueScore;
    private Collection<MatchEvent> matchEvents;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", nullable = false)
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type", nullable = false)
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "number", nullable = false)
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Match match = (Match) o;

        if (type != match.type) return false;
        if (number != match.number) return false;
        return id != null ? id.equals(match.id) : match.id == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + type;
        result = 31 * result + number;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "red_alliance", referencedColumnName = "id", nullable = false)
    public Alliance getBlueAlliance() {
        return blueAlliance;
    }

    public void setBlueAlliance(Alliance blueAlliance) {
        this.blueAlliance = blueAlliance;
    }

    @ManyToOne
    @JoinColumn(name = "blue_alliance", referencedColumnName = "id", nullable = false)
    public Alliance getRedAlliance() {
        return redAlliance;
    }

    public void setRedAlliance(Alliance redAlliance) {
        this.redAlliance = redAlliance;
    }

    @ManyToOne
    @JoinColumn(name = "division", referencedColumnName = "id", nullable = false)
    public Division getDivision() {
        return division;
    }

    public void setDivision(Division division) {
        this.division = division;
    }

    @ManyToOne
    @JoinColumn(name = "red_score", referencedColumnName = "id", nullable = false)
    public Score getRedScore() {
        return redScore;
    }

    public void setRedScore(Score redScore) {
        this.redScore = redScore;
    }

    @ManyToOne
    @JoinColumn(name = "blue_score", referencedColumnName = "id", nullable = false)
    public Score getBlueScore() {
        return blueScore;
    }

    public void setBlueScore(Score blueScore) {
        this.blueScore = blueScore;
    }

    @OneToMany(mappedBy = "match")
    public Collection<MatchEvent> getMatchEvents() {
        return matchEvents;
    }

    public void setMatchEvents(Collection<MatchEvent> matchEvents) {
        this.matchEvents = matchEvents;
    }
}
