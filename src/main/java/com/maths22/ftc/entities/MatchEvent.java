package com.maths22.ftc.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Arrays;
import java.util.UUID;

/**
 * Created by jburroughs on 7/6/16.
 */
@Entity
@Table(name = "match_event", schema = "ftc_scoring", catalog = "")
public class MatchEvent {
    private UUID id;
    private int type;
    private Match match;
    private TeamEventAssignment team;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", nullable = false)
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type", nullable = false)
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MatchEvent that = (MatchEvent) o;

        if (type != that.type) return false;
        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + type;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "match", referencedColumnName = "id", nullable = false)
    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    @ManyToOne
    @JoinColumn(name = "team", referencedColumnName = "id", nullable = false)
    public TeamEventAssignment getTeam() {
        return team;
    }

    public void setTeam(TeamEventAssignment team) {
        this.team = team;
    }
}
