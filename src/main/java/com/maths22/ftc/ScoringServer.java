package com.maths22.ftc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScoringServer {

	public static void main(String[] args) {
		SpringApplication.run(ScoringServer.class, args);
	}


}
