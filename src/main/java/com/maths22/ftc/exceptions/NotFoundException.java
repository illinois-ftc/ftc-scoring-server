package com.maths22.ftc.exceptions;

import com.googlecode.jsonrpc4j.JsonRpcError;
import com.maths22.ftc.RpcException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by jburroughs on 7/6/16.
 */
//@RpcException(123)
public class NotFoundException extends RuntimeException {
}
