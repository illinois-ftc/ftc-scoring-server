package com.maths22.ftc.configuration;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.TypeResolverBuilder;
import com.googlecode.jsonrpc4j.*;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImplExporter;
import com.maths22.ftc.RpcException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;
import java.util.UUID;

//TODO clean up all block comments of this type
/**
 * Created by jburroughs on 7/10/16.
 */
@Configuration
public class RpcConfiguration {

    @Bean
    static public AutoJsonRpcServiceImplExporter jsonServiceExporter() {
        AutoJsonRpcServiceImplExporter ret = new AutoJsonRpcServiceImplExporter();

        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
        TypeResolverBuilder builder = new ObjectMapper.DefaultTypeResolverBuilder(null) {
            @Override
            public boolean useForType(JavaType t) {
                //Code adapted from DefaultTypeResolverBuilder
                while (t.isArrayType()) {
                    t = t.getContentType();
                }
                // [Issue#88] Should not apply to JSON tree models:
                return !t.isContainerType() && !t.isTypeOrSubTypeOf(UUID.class)
                        && !TreeNode.class.isAssignableFrom(t.getRawClass());
            }
        };
        builder.init(JsonTypeInfo.Id.CLASS, null);
        builder.inclusion(JsonTypeInfo.As.PROPERTY);
        mapper.setDefaultTyping(builder);
        ret.setObjectMapper(mapper);
        ret.setHttpStatusCodeProvider(resultCode -> 200);
        MultipleErrorResolver resolver = new MultipleErrorResolver();
        resolver.addErrorResolver((ErrorResolver) (t, method, arguments) -> {
            RpcException exception = t.getClass().getAnnotation(RpcException.class);
            if(exception != null) {
                return new ErrorResolver.JsonError(exception.value(), t.getMessage(), new ErrorData(t.getClass().getName(), t.getMessage()));
            }
            return null;
        });
        resolver.addErrorResolver(DefaultErrorResolver.INSTANCE);
        ret.setErrorResolver(resolver);


        return ret;
    }

}
